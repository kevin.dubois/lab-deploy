import ipaddress
import argparse
import re

# Third-party libraries
from netmiko import Netmiko
import yaml

# Constants
VLAN_START = 10
VLAN_STEP = 5
SSH_USER = "admin"
SSH_PASS = "P@ssw0rd"



def generate_interface_config(interface):
    '''Creates the CLI configuration of the IOS router interfaces'''

    s = '''interface Gi1.{vlan}
    encapsulation dot1q {vlan}
    ip address {ip}
    exit'''.format(vlan=interface['vlan'], ip=interface['address'].with_netmask.replace('/', ' '))
    
    return s


def build_topology(topology_file):
    '''Return a topology dict with the nodes and the subnets automatically generated'''
    global VLAN_START, VLAN_STEP

    vlan_index = VLAN_START
    topology = {
        "nodes": {},
        "subnets": []
    }

    for node in topology_file['topology']:
        # Process each node in topology file
        node_name = node['node']
        topology['nodes'][node['node']] = {
            'name': node['name'],
            'interfaces': []
        }

        for interface in node['interfaces']:
            interface_address = ipaddress.IPv4Interface(interface)
            if interface_address.network in [subnet['network'] for subnet in topology['subnets']]:
                for subnet in topology['subnets']:
                    if interface_address.network == subnet['network']:
                        vlan = subnet['vlan']
                topology['nodes'][node_name]['interfaces'].append({"vlan": vlan, "address": interface_address})
            else:
                topology['subnets'].append({"vlan": vlan_index, "network": interface_address.network})
                topology['nodes'][node_name]['interfaces'].append({"vlan": vlan_index, "address": interface_address})

                vlan_index += VLAN_STEP
    
    return topology

def is_sub_intf(interface):
    '''Check if interface is a sub-interface'''
    regex = re.compile(r'\w+(\/\d{1,3})*\.\d+')

    if regex.match(interface):
        return True
    else:
        return False

def is_loopback(interface):
    if "Loopback" in interface:
        return True
    else:
        return False

def is_tunnel(interface):
    if "Tunnel" in interface:
        return True
    else:
        return False


def configure_node(name, node, devices):
    '''Configure topology node using SSH'''
    
    ssh_con = Netmiko(host=devices['management'][name]['address'], username=SSH_USER, password=SSH_PASS, device_type=devices['management'][name]['type'])

    interfaces = ssh_con.send_command("show ip interface brief", use_textfsm=True)

    config = []

    for interface in interfaces:
        if not interface['ipaddr'] == devices['management'][name]['address']:
            if is_sub_intf(interface['intf']) or is_loopback(interface['intf']) or is_tunnel(interface['intf']):
                print("Deleting sub-interface")
                s = 'no interface {intf}'.format(intf=interface['intf'])
            else:
                print("Resetting {intf}".format(intf=interface['intf']))
                s = '''interface {interface_name}
                    no ip address
                    exit
                '''.format(interface_name=interface['intf'])
            config.append(s)

    
    config.append("hostname {name}".format(name=node['name']))

    for interface in node['interfaces']:
        config.append(generate_interface_config(interface))

    ssh_con.send_config_set(config)
    
    print('{name} configured successfully'.format(name=node['name']))
    print("\n")

def main():
    # CLI parser definition
    parser = argparse.ArgumentParser(description="Configure network topology based on a YAML file")
    parser.add_argument("topology", help="Path to YAML topology file")

    args = parser.parse_args()

    with open('devices.yml', 'r') as ymlfile:
        devices = yaml.load(ymlfile)

    with open(args.topology, 'r') as ymlfile:
        topology_file = yaml.load(ymlfile)
    
    topology = build_topology(topology_file) # Get the topology with VLANs and subnets associated

    for name, node in topology['nodes'].items():
        print("=== {name} ({node})===".format(name=node['name'], node=name))
        configure_node(name, node, devices)
    
    print("Topology fully configured")

if __name__ == "__main__":
    main()